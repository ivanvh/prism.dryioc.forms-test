﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamFormsPrism.Constants
{
    public class NavigationLinks
    {
        public const string MainPage = "MainPage";
        public const string EmployeeRegister = "EmployeeRegister";
        public const string StudentRegister = "StudentRegister";
        public const string ComercialRegister = "ComercialRegister";
    }
}
