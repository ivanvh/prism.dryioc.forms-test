﻿using Prism;
using Prism.DryIoc;
using Prism.Navigation;
using XamFormsPrism.Constants;
using XamFormsPrism.ViewModels;
using XamFormsPrism.Views;

namespace XamFormsPrism
{
    public partial class App : Prism.DryIoc.PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            this.InitializeComponent();

            //NavigationService.NavigateAsync("MainPage?title=Hello%20from%20Xamarin.Forms");
            NavigationParameters param = new NavigationParameters();
            param.Add("title", "Halo Params");
            this.NavigationService.NavigateAsync(NavigationLinks.MainPage, param);            
        }

        protected override void RegisterTypes()
        {
            this.Container.RegisterTypeForNavigation<MainPage>();
            this.Container.RegisterTypeForNavigation<ComercialRegister>();
            this.Container.RegisterTypeForNavigation<StudentRegister>();
            this.Container.RegisterTypeForNavigation<EmployeeRegister>();

            //this.Container.Register<MainPageViewModel, MainPageViewModel>();
        }
    }
}
