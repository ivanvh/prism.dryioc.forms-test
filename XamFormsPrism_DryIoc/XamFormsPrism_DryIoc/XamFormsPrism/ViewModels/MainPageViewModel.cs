﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using XamFormsPrism.Constants;

namespace XamFormsPrism.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        private INavigationService navigationService = null;

        private string employeeText = "E";
        public string EmployeeText
        {
            get { return employeeText; }
            set { SetProperty(ref employeeText, value); }
        }

        private string studentText = "S";
        public string StudentText
        {
            get { return studentText; }
            set { SetProperty(ref studentText, value); }
        }

        private string commercialText = "C";
        public string CommercialText
        {
            get { return commercialText; }
            set { SetProperty(ref commercialText, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public DelegateCommand NavigateCommand { get; private set; }

        public DelegateCommand EmployeeNavigateCommand { get; private set; }

        public DelegateCommand ComercialNavigateCommand { get; private set; }

        public DelegateCommand StudentNavigateCommand { get; private set; }


        public MainPageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.EmployeeNavigateCommand = new DelegateCommand(this.employeeNavigate, canNavigate);
            this.ComercialNavigateCommand = new DelegateCommand(this.comercialNavigate, canNavigate);
            this.StudentNavigateCommand = new DelegateCommand(this.studentNavigate, canNavigate);

            this.StudentText = "Studnetss";
            this.CommercialText = "Comercialss";
            this.EmployeeText = "Employeess";
        }

        private bool canNavigate()
        {
            return true;
        }

        private void employeeNavigate()
        {
            this.navigationService.NavigateAsync(NavigationLinks.EmployeeRegister);
        }

        private void comercialNavigate()
        {
            this.navigationService.NavigateAsync(NavigationLinks.ComercialRegister);
        }

        private void studentNavigate()
        {
            this.navigationService.NavigateAsync(NavigationLinks.StudentRegister);
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("title"))
                Title = (string)parameters["title"] + " and Prism";
        }

    }
}
