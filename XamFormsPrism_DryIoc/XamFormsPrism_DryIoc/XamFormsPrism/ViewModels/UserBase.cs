﻿using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamFormsPrism.ViewModels
{
    public abstract class UserBase : BindableBase, INavigationAware
    {
        private INavigationService navigationService = null;

        public abstract string Title { get; }

        private string fisrtname;
        public string Fisrtname
        {
            get { return fisrtname; }
            set { SetProperty(ref fisrtname, value); }
        }

        private string lastname;
        public string Lastname
        {
            get { return lastname; }
            set { SetProperty(ref lastname, value); }
        }

        private string idPassport;
        public string IdPassport
        {
            get { return idPassport; }
            set { SetProperty(ref idPassport, value); }
        }

        private string mobileNumber;
        public string MobileNumber
        {
            get { return mobileNumber; }
            set { SetProperty(ref mobileNumber, value); }
        }

        public UserBase(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        public abstract void OnNavigatedFrom(NavigationParameters parameters);

        public abstract void OnNavigatedTo(NavigationParameters parameters);
    }
}
