﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;

namespace XamFormsPrism.ViewModels
{
    public class EmployeeRegisterViewModel : UserBase
    {
        public EmployeeRegisterViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }

        public override string Title
        {
            get
            {
                return "Employee Registration";
            }
        }
        

        public override void OnNavigatedFrom(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
    }
}
